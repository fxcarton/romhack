# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GP32 example code"
HOMEPAGE="https://github.com/devkitpro/gp32-examples"
SRC_URI="https://github.com/devkitpro/gp32-examples/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="arm"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[gp32]"

src_install() {
	dodir /usr/bin
	find . -type f -name '*.fxe' -exec cp '{}' "${D}/usr/bin/" ';' || die
}
