# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="Game Raster Image Transmogrifier"
HOMEPAGE="https://github.com/devkitPro/grit"
SRC_URI="https://github.com/devkitPro/grit/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="|| ( GPL-2 MIT FIPL-1.0 )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND="media-libs/freeimage:0=[png(+)]"
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
	eapply_user
}
