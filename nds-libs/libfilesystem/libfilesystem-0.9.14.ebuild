# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Nintendo DS filesystem support library"
HOMEPAGE="https://github.com/devkitPro/libfilesystem"
SRC_URI="https://github.com/devkitPro/libfilesystem/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[nds]"
DEPEND="nds-libs/libnds nds-libs/libfat"
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*.a
}
