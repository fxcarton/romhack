# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C library for Nintendo DS"
HOMEPAGE="https://github.com/devkitPro/libnds"
SRC_URI="${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[nds] media-gfx/grit"
DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*
	insinto /usr/share
	doins icon.bmp
}
