# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="NDS audio library"
HOMEPAGE="https://github.com/devkitPro/maxmod"
SRC_URI="https://github.com/devkitPro/maxmod/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[nds]"
DEPEND=""
RDEPEND="${DEPEND}"

src_compile() {
	emake ds7 ds9
}

src_install() {
	doheader include/maxmod7.h include/maxmod9.h include/mm_types.h
	dolib.a lib/libmm7.a lib/libmm9.a
}
