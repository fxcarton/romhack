# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Wifi library for Nintendo DS"
HOMEPAGE="https://github.com/devkitPro/dswifi"
SRC_URI="https://github.com/devkitPro/dswifi/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[nds]"
DEPEND="nds-libs/libnds"
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*
}
