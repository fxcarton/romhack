# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Library for drawing 2D graphics using the Nintendo 3DS's PICA200 GPU"
HOMEPAGE="https://github.com/devkitPro/citro2d"
SRC_URI="https://github.com/devkitPro/citro2d/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[3ds]"
DEPEND="3ds-libs/libctru >=3ds-libs/citro3d-1.4.0"
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*.a
}
