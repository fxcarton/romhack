# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C library for writing user mode arm11 code for the 3DS (CTR)"
HOMEPAGE="https://github.com/devkitPro/libctru"
SRC_URI="https://github.com/devkitPro/libctru/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[3ds]"
DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}/libctru"

src_install() {
	doheader -r include/*
	dolib.a lib/*.a
	insinto /usr/share
	doins default_icon.png
}
