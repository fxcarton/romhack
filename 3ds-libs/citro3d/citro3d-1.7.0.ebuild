# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Homebrew PICA200 GPU wrapper library for Nintendo 3DS "
HOMEPAGE="https://github.com/devkitPro/citro3d"
SRC_URI="https://github.com/devkitPro/citro3d/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[3ds]"
DEPEND=">=3ds-libs/libctru-1.5.0"
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*.a
}
