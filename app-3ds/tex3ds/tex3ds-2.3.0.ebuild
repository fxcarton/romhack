# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="3DS Texture Conversion"
HOMEPAGE="https://github.com/devkitPro/tex3ds"
SRC_URI="https://github.com/devkitPro/tex3ds/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="+lto"

DEPEND="media-libs/freetype:2
	>=media-gfx/imagemagick-6.0.0:=[cxx]"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

src_prepare() {
	sed -i 's/ -pipe//' configure.ac || die
	use lto || sed -i 's/\<-flto\>//' configure.ac || die
	eautoreconf
	eapply_user
}
