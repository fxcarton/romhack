# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="3DS homebrew tools (3dsxtool, 3dsxdump, smdhtool)"
HOMEPAGE="https://github.com/devkitPro/3dstools"
SRC_URI="https://github.com/devkitPro/3dstools/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/3dstools-${PV}"

src_prepare() {
	eautoreconf
	eapply_user
}
