# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="An all-in-one tool for extracting/creating 3ds roms"
HOMEPAGE="https://github.com/3DSGuy/Project_CTR"
SRC_URI="https://github.com/3DSGuy/Project_CTR/archive/refs/tags/${PN}-v${PV}.tar.gz -> Project_CTR-${PN}-v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

BDEPEND="virtual/pkgconfig"
DEPEND="dev-libs/tinyxml net-libs/mbedtls:="
RDEPEND="${DEPEND}"

S="${WORKDIR}/Project_CTR-${PN}-v${PV}/${PN}"

src_prepare() {
	eapply_user

	# respect flags
	sed -e 's/\<-O2\>//' \
		-e 's/^\(CFLAGS\) *=/\1 +=/' \
		-e 's/^\(CXXFLAGS\) *=/\1 +=/' \
		-e 's/\<gcc/$(CC)/g' \
		-e 's/\<g++/$(CXX)/g' \
		-e '/^CC[ \t]*=/d' \
		-e '/^CXX[ \t]*=/d' \
		-e '/^\t\$(CXX)/s/\$(CXX) /&$(LDFLAGS) /' \
		-i Makefile || die
	export CC="$(tc-getCC)"
	export CXX="$(tc-getCXX)"
	export CFLAGS
	export CXXFLAGS
	export LDFLAGS
	export LIBS

	# unbundle tinyxml
	rm -r tinyxml || die
	sed '/^SRC_DIR/s/\<tinyxml\>//' -i Makefile || die
	sed 's:tinyxml/\(tinyxml\.h\):\1:' -i keyset.cpp || die

	# unbundle polarssl
	rm -r polarssl || die
	sed '/^SRC_DIR/s/\<polarssl\>//' -i Makefile || die
	sed 's:polarssl/\(.*\.h\):mbedtls/\1:;s:mbedtls/sha2\.h:mbedtls/sha256.h:' -i ctr.h || die
	sed -e 's/\<aes_context\>/mbedtls_&/g' \
		-e 's/\<rsa_context\>/mbedtls_&/g' \
		-e 's/\<sha2_context\>/mbedtls_sha256_context/g' \
		-e 's/\<aes_setkey_enc\>/mbedtls_&/g' \
		-e 's/\<aes_setkey_dec\>/mbedtls_&/g' \
		-e 's/\<aes_crypt_ecb\>/mbedtls_&/g' \
		-e 's/\<aes_crypt_cbc\>/mbedtls_&/g' \
		-e 's/\<AES_ENCRYPT\>/MBEDTLS_&/g' \
		-e 's/\<AES_DECRYPT\>/MBEDTLS_&/g' \
		-e 's/\<sha2\>/mbedtls_sha256_ret/g' \
		-e 's/\<sha2_starts\>/mbedtls_sha256_starts_ret/g' \
		-e 's/\<sha2_update\>/mbedtls_sha256_update_ret/g' \
		-e 's/\<sha2_finish\>/mbedtls_sha256_finish_ret/g' \
		-e 's/\<rsa_init\>/mbedtls_&/g' \
		-e 's/\<rsa_check_pubkey\>/mbedtls_&/g' \
		-e 's/\<rsa_check_privkey\>/mbedtls_&/g' \
		-e 's/\<rsa_public\>/mbedtls_&/g' \
		-e 's/\<rsa_free\>/mbedtls_&/g' \
		-e 's/\<rsa_pkcs1_verify\>([^,]*,/mbedtls_&0, 0,/g' \
		-e 's/\<rsa_pkcs1_sign\>([^,]*,/mbedtls_&0, 0,/g' \
		-e 's/\<RSA_PUBLIC\>/MBEDTLS_&/g' \
		-e 's/\<RSA_PRIVATE\>/MBEDTLS_&/g' \
		-e 's/\<RSA_PKCS_V15\>/MBEDTLS_&/g' \
		-e 's/\<SIG_RSA_SHA256\>/MBEDTLS_MD_SHA256/g' \
		-e 's/\<mpi_read_binary\>/mbedtls_&/g' \
		-i *.c *.h || die

	# use pkg-config for deps
	local deps="tinyxml"
	local cflags_deps="$($(tc-getPKG_CONFIG) --cflags $deps)"
	local ldflags_deps="$($(tc-getPKG_CONFIG) --libs-only-L --libs-only-other $deps)"
	local libs_deps="$($(tc-getPKG_CONFIG) --libs-only-l --libs-only-other $deps)"
	CFLAGS="${CFLAGS} ${cflags_deps}"
	CXXFLAGS="${CXXFLAGS} ${cflags_deps}"
	LDFLAGS="${LDFLAGS} ${ldflags_deps}"
	LIBS="${LIBS} ${libs_deps} -lmbedcrypto"
}

src_install() {
	dobin ${PN}
}
