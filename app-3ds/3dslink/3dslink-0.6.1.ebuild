# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="Tool to send homebrew to 3DS"
HOMEPAGE="https://github.com/devkitPro/3dslink"
SRC_URI="https://github.com/devkitPro/3dslink/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

BDEPEND="virtual/pkgconfig"
DEPEND="sys-libs/zlib:0="
RDEPEND="${DEPEND}"

S="${S}/host"

src_prepare() {
	eautoreconf
	eapply_user
}
