# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A tool for creating 3DS banners"
HOMEPAGE="https://github.com/Steveice10/bannertool"
BUILDTOOLS_COMMIT="4524b3a324ae8e9dcaf80c4fe3694bc63628de22"
SRC_URI="https://github.com/Steveice10/bannertool/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
		 https://github.com/Steveice10/buildtools/archive/${BUILDTOOLS_COMMIT}.zip -> steveice10-buildtools-${BUILDTOOLS_COMMIT}.zip"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_unpack() {
	default
	rmdir ${P}/buildtools
	mv buildtools-* ${P}/buildtools
}

src_compile() {
	emake VERSION_PARTS="$(echo ${PV} | sed 's/\./ /g')" output/linux-$(uname -m)/${PN}
}

src_install() {
	dobin output/linux-$(uname -m)/${PN}
}
