# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="An all-in-one tool for extracting/creating 3ds roms"
HOMEPAGE="https://github.com/dnasdw/3dstool"
SRC_URI="https://github.com/dnasdw/3dstool/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND="dev-libs/openssl:0= net-misc/curl:0= dev-libs/capstone:0="
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${PN}-1.0.26-rpath-fix.patch"
	"${FILESDIR}/${PN}-1.2.6-proper-deps-with-pkg-config.patch"
)

src_prepare() {
	rm -rf dep || die # make sure no bundled deps are used
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=("-DUSE_DEP=OFF")
	cmake_src_configure
}
