# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="An all-in-one tool for extracting/creating 3ds roms"
HOMEPAGE="https://github.com/3DSGuy/Project_CTR"
SRC_URI="https://github.com/3DSGuy/Project_CTR/archive/refs/tags/${PN}-v${PV}.tar.gz -> Project_CTR-${PN}-v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/Project_CTR-${PN}-v${PV}/${PN}"

src_prepare() {
	sed -e "s/ -O2/ ${CFLAGS}/" -e "s/gcc/$(tc-getCC)/" -e 's/\$(LIBS)/$(LDFLAGS)/' -i Makefile || die "Makefile patch failed"
	eapply_user
}

src_install() {
	dobin ${PN}
}
