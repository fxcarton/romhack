# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C Library for Nintendo GBA"
HOMEPAGE="https://github.com/devkitPro/libgba"
SRC_URI="https://github.com/devkitPro/libgba/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="libgba"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[gba]"
DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*.a
}
