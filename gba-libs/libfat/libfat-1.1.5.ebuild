# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="FAT library for GBA"
HOMEPAGE="https://github.com/devkitPro/libfat"
SRC_URI="https://github.com/devkitPro/libfat/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[gba]"
DEPEND="gba-libs/libgba"
RDEPEND="${DEPEND}"

src_compile() {
	emake gba-release
}

src_install() {
	doheader include/fat.h include/libfatversion.h
	dolib.a gba/lib/libfat.a
}
