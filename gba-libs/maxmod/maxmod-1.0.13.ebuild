# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GBA audio library"
HOMEPAGE="https://github.com/devkitPro/maxmod"
SRC_URI="https://github.com/devkitPro/maxmod/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[gba]"
DEPEND=""
RDEPEND="${DEPEND}"

src_compile() {
	emake gba
}

src_install() {
	doheader include/maxmod.h include/mm_types.h
	dolib.a lib/libmm.a
}
