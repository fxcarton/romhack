# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Patches to libraries for devkitARM platforms"
HOMEPAGE="https://github.com/devkitPro/pacman-packages"
COMMIT="ef87c316edf3df5b44e957d628b5fac276e9a1d8"
SRC_URI="https://github.com/devkitPro/pacman-packages/archive/${COMMIT}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/pacman-packages-${COMMIT}"

copy_patches() {
	local -A cat=(
		[bulletphysics]=sci-physics
		[curl]=net-misc
		[flac]=media-libs
		[libarchive]=app-arch
		[libconfig]=dev-libs
		[libjpeg-turbo]=media-libs
		[libmad]=media-libs
		[libmodplug]=media-libs
		[libsidplay]=media-libs
		[libtheora]=media-libs
		[mbedtls]=net-libs
		[mpg123]=media-sound
		[physfs]=dev-games
		[SDL]=media-libs
	)
	local -A pno=(
		[bulletphysics]=bullet
		[SDL]=libsdl
	)
	target="$1"
	dest="${DEVKITPRO}/portlibs/$target/etc/portage/patches"
	shift
	for f in $(find "$@" -type f -name '*.patch'); do
		d="${f%/*}"
		dn="${d##*/}"
		n="${f##*/}"
		n="${n#${target}-}"
		pv="${n%.patch}"
		pn="${pno["$dn"]}"
		test -n "$pn" || pn="${pv%-*}"
		c="${cat["$dn"]}"
		if test -z "$c"; then
			ewarn "Skipping patch '$f' due to unknown package"
			continue
		fi
		insinto "$dest/$c/$pv"
		newins "$f" "devkitarm-$pv.patch"
		einfo "$f -> devkitarm-$pv.patch"
	done
}

src_install() {
	copy_patches nds armv4t nds
	copy_patches 3ds armv4t 3ds
}
