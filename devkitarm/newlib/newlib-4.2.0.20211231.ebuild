# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DEVKITARM_VER=57
inherit devkitarm out-of-source

DESCRIPTION="Newlib is a C library intended for use on embedded systems"
HOMEPAGE="https://sourceware.org/newlib/"
SRC_URI="ftp://sourceware.org/pub/newlib/${P}.tar.gz
	$(devkitpro_buildscripts_uri)"

LICENSE="NEWLIB LIBGLOSS GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""
RESTRICT="strip"
CHOST="arm-none-eabi"

DEPEND="devkitarm/gcc"
RDEPEND="${DEPEND}"

src_prepare() {
	devkitpro_apply_patch
	default
}

my_src_configure() {
	export PATH="${DEVKITARM}/bin:${PATH}"
	unset CFLAGS
	unset CXXFLAGS
	unset CC
	unset CXX
	CFLAGS_FOR_TARGET="-O2 -ffunction-sections -fdata-sections" \
	"${S}/configure" --disable-newlib-supplied-syscalls --enable-newlib-mb --disable-newlib-wide-orient --target=arm-none-eabi --prefix=${DEVKITARM}
}

my_src_compile() {
	export PATH="${DEVKITARM}/bin:${PATH}"
	emake
}

my_src_install() {
	export PATH="${DEVKITARM}/bin:${PATH}"
	emake -j1 DESTDIR="${D}" install
}

my_src_install_all() {
	:
}
