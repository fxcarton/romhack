# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit devkitarm

DESCRIPTION="devkitARM wrapper for pkg-config, via crossdev"
HOMEPAGE="https://framagit.org/fxcarton/romhack"
SRC_URI=""

LICENSE="metapackage"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="sys-devel/crossdev"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"

src_install() {
	dosym "/usr/bin/cross-pkg-config" "${DEVKITARM}/bin/arm-none-eabi-pkg-config"
}
