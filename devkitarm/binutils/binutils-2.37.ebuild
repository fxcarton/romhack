# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DEVKITARM_VER=57
inherit devkitarm out-of-source

DESCRIPTION="Tools necessary to build programs"
HOMEPAGE="https://sourceware.org/binutils/"
SRC_URI="mirror://gnu/binutils/binutils-${PV}.tar.xz
	$(devkitpro_buildscripts_uri)"

LICENSE="|| ( GPL-3 LGPL-3 )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND="sys-libs/zlib"
RDEPEND="${DEPEND}"

src_prepare() {
	devkitpro_apply_patch
	default
}

my_src_configure() {
	"${S}/configure" --prefix=${DEVKITARM} --target=arm-none-eabi --disable-nls --disable-werror --enable-lto --enable-plugins --enable-poison-system-directories --build=${CBUILD}
}

my_src_compile() {
	emake
}

my_src_install() {
	emake DESTDIR="${D}" install
	rm "${D}/${DEVKITARM}/share/info/dir"
}

my_src_install_all() {
	:
}
