# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit devkitarm

DESCRIPTION="devkitARM crossdev"
HOMEPAGE="https://framagit.org/fxcarton/romhack"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="+gp32 +gba +nds +3ds"

DEPEND=""
RDEPEND="sys-devel/crossdev"
BDEPEND=""

S="${FILESDIR}"

src_install() {
	dobin "${FILESDIR}/dkarm-emerge"

	local profiles=() p root configroot d
	local thisrepo="$(realpath "$(realpath "${FILESDIR}"/../../..)")"
	use gp32 && profiles+=(gp32)
	use gba && profiles+=(gba)
	use nds && profiles+=(nds)
	use 3ds && profiles+=(3ds)
	for p in "${profiles[@]}"; do
		configroot="${DEVKITPRO}/portlibs/${p}"
		root="${configroot}"
		d="${configroot}/etc/portage"
		dosym "${thisrepo}/profiles/devkitarm/${p}" "${d}/make.profile"
		dosym "${PORTAGE_CONFIGROOT%/}/etc/portage/repos.conf" "${d}/repos.conf"
		printf 'FEATURES="-news noman nodoc"\nROOT="%s"\n' "${root}" > "${D}/${d}/make.conf" || die
	done
}
