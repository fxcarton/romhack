# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DEVKITARM_VER=57
inherit devkitarm out-of-source

NEWLIB_VER=4.2.0.20211231
DESCRIPTION="The GNU Compiler Collection"
HOMEPAGE="https://gcc.gnu.org/"
SRC_URI="mirror://gnu/gcc/gcc-${PV}/gcc-${PV}.tar.xz
	$(devkitpro_buildscripts_uri)"

LICENSE="GPL-3+ LGPL-3+ || ( GPL-3+ libgcc libstdc++ gcc-runtime-library-exception-3.1 ) FDL-1.3+"
SLOT="${PV}"
KEYWORDS="amd64 ~x86"
IUSE="stage1"

DEPEND="devkitarm/binutils
	sys-libs/zlib
	stage1? ( ~devkitarm/newlib-headers-${NEWLIB_VER} )
	!stage1? ( ~devkitarm/newlib-${NEWLIB_VER} )"
RDEPEND="${DEPEND}"

QA_PREBUILT="${DEVKITARM#/}/lib/* ${DEVKITARM#/}/lib64/* ${DEVKITARM#/}/arm-none-eabi/lib/*"

src_prepare() {
	devkitpro_apply_patch
	default
}

my_src_configure() {
	export PATH="${DEVKITARM}/bin:${PATH}"
	CFLAGS_FOR_TARGET="-O2 -ffunction-sections -fdata-sections" \
	CXXFLAGS_FOR_TARGET="-O2 -ffunction-sections -fdata-sections" \
	LDFLAGS_FOR_TARGET="" \
	"${S}/configure" \
		--enable-languages=c,c++,objc,lto \
		--with-gnu-as --with-gnu-ld --with-gcc \
		--with-march=armv4t \
		--enable-cxx-flags='-ffunction-sections' \
		--disable-libstdcxx-verbose \
		--enable-poison-system-directories \
		--enable-interwork --enable-multilib \
		--enable-threads --disable-win32-registry --disable-nls --disable-debug \
		--disable-libmudflap --disable-libssp --disable-libgomp \
		--disable-libstdcxx-pch \
		--enable-libstdcxx-time=yes \
		--enable-libstdcxx-filesystem-ts \
		--target=arm-none-eabi \
		--with-newlib \
		--with-headers=yes \
		--prefix=${DEVKITARM} \
		--enable-lto \
		--with-system-zlib \
		--disable-tm-clone-registry \
		--disable-__cxa_atexit \
		--with-bugurl="https://framagit.org/fxcarton/romhack/-/issues" --with-pkgversion="devkitARM release ${DEVKITARM_VER}" \
		--build=${CBUILD}
}

my_src_compile() {
	export PATH="${DEVKITARM}/bin:${PATH}"
	emake all-gcc
	use stage1 || emake all
}

my_src_install() {
	export PATH="${DEVKITARM}/bin:${PATH}"
	emake DESTDIR="${D}" "$(usex stage1 install-gcc install)"
	rm "${D}/${DEVKITARM}/share/info/dir"
	dostrip -x "${DEVKITARM}/lib" "${DEVKITARM}/lib64" "${DEVKITARM}/arm-none-eabi/lib"
}

my_src_install_all() {
	:
}

pkg_postinst() {
	if ! use stage1; then
		einfo "To have a working compiler, remove the sys-include directory with:"
		einfo "emerge -ac devkitarm/newlib-headers"
	fi
}
