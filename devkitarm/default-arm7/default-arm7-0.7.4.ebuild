# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Default arm7 code for NDS roms"
HOMEPAGE="https://github.com/devkitPro/default-arm7"
SRC_URI="https://github.com/devkitPro/default-arm7/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[nds]"
DEPEND=">=nds-libs/libnds-1.7.0 nds-libs/dswifi nds-libs/maxmod"
RDEPEND="${DEPEND}"

src_install() {
	dobin default.elf
}
