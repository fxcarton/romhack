# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DEVKITARM_VER=57
inherit devkitarm

MY_P="newlib-${PV}"
DESCRIPTION="Newlib is a C library intended for use on embedded systems"
HOMEPAGE="https://sourceware.org/newlib/"
SRC_URI="ftp://sourceware.org/pub/newlib/${MY_P}.tar.gz
	$(devkitpro_buildscripts_uri)"

LICENSE="NEWLIB LIBGLOSS GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""
RESTRICT="strip"
CHOST="arm-none-eabi"

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	devkitpro_apply_patch "newlib-${PV}.patch"
	default
}

src_configure() {
	:
}

src_compile() {
	:
}

src_install() {
	insinto "${DEVKITARM}/arm-none-eabi/sys-include"
	doins -r "${S}/newlib/libc/include"/*
}
