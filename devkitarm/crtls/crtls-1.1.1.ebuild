# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit devkitarm

DESCRIPTION="devkitARM crtls"
HOMEPAGE="https://github.com/devkitpro/devkitarm-crtls"
SRC_URI="https://github.com/devkitpro/devkitarm-crtls/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MPL-2.0 gba? ( public-domain ) gp32? ( public-domain )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="+gp32 +gba +nds +3ds"
RESTRICT="strip"

BDEPEND="devkitarm/rules[gp32?,gba?,nds?,3ds?]"
DEPEND=""
RDEPEND="${DEPEND}"

CHOST="arm-none-eabi"
S="${WORKDIR}/devkitarm-${P}"

src_compile() {
	mkdir -p thumb armv6k/fpu
	use gp32 && emake gp32_crt0.o thumb/gp32_crt0.o gp32_gpsdk_crt0.o thumb/gp32_gpsdk_crt0.o
	use gba && emake gba_crt0.o thumb/gba_crt0.o er_crt0.o thumb/er_crt0.o
	use nds && emake ds_arm7_vram_crt0.o thumb/ds_arm7_vram_crt0.o ds_arm7_crt0.o thumb/ds_arm7_crt0.o ds_arm9_crt0.o thumb/ds_arm9_crt0.o
	use 3ds && emake armv6k/fpu/3dsx_crt0.o
}

src_install() {
	insinto "${DEVKITARM}/arm-none-eabi/lib"

	use gp32 && doins gp32.ld gp32.specs gp32_gpsdk.ld gp32_gpsdk.specs
	use gba && doins gba_cart.ld gba.specs gba_er.ld gba_er.specs gba_mb.ld gba_mb.specs
	use nds && doins ds_cart.ld ds_cart.specs ds_arm7.ld ds_arm7.specs ds_arm9.ld ds_arm9.specs ds_arm9.mem \
					 ds_arm7_iwram.ld ds_arm7_iwram.specs ds_arm7_vram.ld ds_arm7_vram.specs dsi_arm9.specs dsi_arm9.mem dldi.ld
	use 3ds && doins 3dsx.ld 3dsx.specs

	use gp32 && doins gp32_crt0.o gp32_gpsdk_crt0.o
	use gba && doins gba_crt0.o er_crt0.o
	use nds && doins ds_arm7_crt0.o ds_arm9_crt0.o ds_arm7_vram_crt0.o

	insinto "${DEVKITARM}/arm-none-eabi/lib/thumb"
	use gp32 && doins thumb/gp32_crt0.o thumb/gp32_gpsdk_crt0.o
	use gba && doins thumb/gba_crt0.o thumb/er_crt0.o
	use nds && doins thumb/ds_arm7_crt0.o thumb/ds_arm9_crt0.o thumb/ds_arm7_vram_crt0.o

	insinto "${DEVKITARM}/arm-none-eabi/lib/armv6k/fpu"
	use 3ds && doins armv6k/fpu/3dsx_crt0.o
}
