# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GBA, NDS, 3DS and GP32 development kit"
HOMEPAGE="https://devkitpro.org"
SRC_URI=""

LICENSE="metapackage"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="+gp32 +gba +nds +3ds +gdb"

RDEPEND="
	~devkitarm/devkitarm-base-57[gp32?,gba?,nds?,3ds?]
	devkitarm/crossdev[gp32?,gba?,nds?,3ds?]
	>=devkitarm/general-tools-1.2.0
	gp32? (
		>=app-gp32/gp32-tools-1.0.3
	)
	gba? (
		>=app-gba/gba-tools-1.1.0
		>=media-gfx/grit-0.8.15
		>=media-sound/mmutil-1.8.7
	)
	nds? (
		>=app-nds/ds-tools-1.2.1
		>=media-gfx/grit-0.8.15
		>=app-nds/ndstool-2.1.1
		>=media-sound/mmutil-1.8.7
	)
	3ds? (
		>=app-3ds/3ds-tools-1.1.4
		>=app-3ds/picasso-2.7.0
		>=app-3ds/3dslink-0.5.2
		>=app-3ds/tex3ds-1.0.0
	)
	gdb? ( >=devkitarm/gdb-11.2 )
	"
