# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="devkitARM base system (makefiles, c runtime, toolchain)"
HOMEPAGE="https://devkitpro.org"
SRC_URI=""

LICENSE="metapackage"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="+gp32 +gba +nds +3ds"

DEPEND=""
RDEPEND=">=devkitarm/binutils-2.37
	>=devkitarm/gcc-11.2.0[-stage1]
	>=devkitarm/newlib-4.2.0.20211231
	>=devkitarm/rules-1.2.1[gp32?,gba?,nds?,3ds?]
	>=devkitarm/crtls-1.1.1[gp32?,gba?,nds?,3ds?]"
