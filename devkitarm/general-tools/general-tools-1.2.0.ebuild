# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit devkitarm autotools

DESCRIPTION="devkitARM general tools"
HOMEPAGE="https://github.com/devkitPro/general-tools"
SRC_URI="https://github.com/devkitPro/general-tools/archive/c1c3b1547d2ba48dd221553f1d62167cadc70d28.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/general-tools-c1c3b1547d2ba48dd221553f1d62167cadc70d28"

src_prepare() {
	eautoreconf
	eapply_user
}

src_configure() {
	econf --prefix="${DEVKITARM}"
}
