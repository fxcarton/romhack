# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit devkitarm out-of-source

BS_VER=20191211
DESCRIPTION="GNU debugger"
HOMEPAGE="https://sourceware.org/gdb/"
SRC_URI="mirror://gnu/gdb/${P}.tar.xz
	https://github.com/devkitPro/binutils-gdb/commit/c616d914f7a04af78e15a6c5e7a8fcbfb631096a.patch -> devkitARM-gdb-${PV}.patch"

LICENSE="GPL-2 LGPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND="devkitarm/gcc[-stage1]"
RDEPEND="${DEPEND}"

PATCHES=( "${DISTDIR}/devkitARM-gdb-${PV}.patch" )

my_src_configure() {
	"${S}/configure" --disable-nls --prefix=${DEVKITARM} --target=arm-none-eabi --disable-werror
}

my_src_install() {
	emake DESTDIR="${D}" install
	rm "${D}/${DEVKITARM}/share/info/dir"
	rm "${D}/${DEVKITARM}/share/info/bfd.info"
}

my_src_install_all() {
	:
}
