# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit devkitarm

DESCRIPTION="devkitARM build rules"
HOMEPAGE="https://github.com/devkitpro/devkitarm-rules"
SRC_URI="https://github.com/devkitPro/devkitarm-rules/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="+gp32 +gba +nds +3ds"

DEPEND=""
RDEPEND="devkitarm/gcc[-stage1] >=devkitarm/general-tools-1.2.0"

S="${WORKDIR}/devkitarm-${P}"

src_prepare() {
	default

	# Update default paths
	# - portlibs are installed in ${ROOT}/usr and not ${ROOT}
	# - there is no reason to install system libs (eg. libgba, libnds, ...) in separate directories
	# - the default [n3]ds icons are moved to /usr/share to avoid QA warnings
	local -A syslibs=(
		[gp32]=libmirko
		[gba]=libgba
		[nds]=libnds
		[3ds]=libctru
	)
	local rulespath
	for t in gp32 gba nds 3ds; do
		rulespath="${t}_rules"
		[ "${t}" = nds ] && rulespath="ds_rules"
		sed -i \
			-e "s,\\\$(PORTLIBS_PATH)/${t},&/usr &,g" \
			-e "s,\\\$(DEVKITPRO)/${syslibs[$t]},\$(PORTLIBS_PATH)/${t}/usr,g" \
			"$rulespath" || die
	done
	sed -i 's,/icon.bmp,/share&,' ds_rules || die
	sed -i 's,/default_icon.png,/share&,' 3ds_rules || die
}

src_compile() {
	:
}

src_install() {
	insinto "${DEVKITARM}"
	doins base_rules base_tools
	use gp32 && doins gp32_rules
	use gba && doins gba_rules
	use nds && doins ds_rules
	use 3ds && doins 3ds_rules
	doenvd "${FILESDIR}/99devkitarm"
}
