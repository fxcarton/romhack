# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="GP32 homebrew tools (b2fxec, gpd, unfxe, zda_compressor, zda_tester)"
HOMEPAGE="https://github.com/devkitPro/gp32-tools"
SRC_URI="https://github.com/devkitPro/gp32-tools/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND=">=sys-libs/zlib-1.2.8:0="
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
	eapply_user
	rm -rf b2fxec gpd unfxe zda_compressor
}
