# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Nintendo GBA example code"
HOMEPAGE="https://github.com/devkitpro/gba-examples"
SRC_URI="https://github.com/devkitpro/gba-examples/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="arm"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[gba]"

src_install() {
	dodir /usr/bin
	find . -type f -name '*.gba' -exec cp '{}' "${D}/usr/bin/" ';' || die
}
