# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="Tool to create/extract nds roms"
HOMEPAGE="https://github.com/devkitPro/ndstool"
SRC_URI="https://github.com/devkitPro/ndstool/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

PATCHES=( "${FILESDIR}/ndstool-default-arm7-path.patch" )

src_prepare() {
	default
	eautoreconf
}
