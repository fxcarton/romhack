# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="NDS homebrew tools (dlditool, r4denc, dslink, nds_texcompress)"
HOMEPAGE="https://github.com/devkitPro/dstools"
SRC_URI="https://github.com/devkitPro/dstools/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/devkitPro/dslink-host/archive/607e132d2415ef06f01a6012c622aaa2c0eccf65.zip -> dslink-${PV}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND="media-libs/freeimage:0=[png(+)] sys-libs/zlib:0="
RDEPEND="${DEPEND}"

S="${WORKDIR}/dstools-${PV}"

src_unpack() {
	default
	rmdir "${S}/src/dslink" || die
	mv "dslink-host-607e132d2415ef06f01a6012c622aaa2c0eccf65" "${S}/src/dslink" || die
}

src_prepare() {
	eautoreconf
	eapply_user
}
