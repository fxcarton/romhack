# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="FTP server"
HOMEPAGE="https://github.com/mtheall/ftpd"
SRC_URI="https://github.com/mtheall/ftpd/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="arm"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[nds]"

src_compile() {
	emake release-nds
}

src_install() {
	dobin nds/ftpd.nds
}
