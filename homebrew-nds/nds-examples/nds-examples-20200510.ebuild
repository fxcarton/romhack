# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Nintendo NDS example code"
HOMEPAGE="https://github.com/devkitpro/nds-examples"
SRC_URI="https://github.com/devkitPro/nds-examples/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="arm"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[nds]"

src_install() {
	dobin bin/*.nds
}
