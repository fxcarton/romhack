# @ECLASS: devkitarm.eclass

DEVKITPRO_PACKAGE="devkitARM"
DEVKITPRO_PACKAGE_VER="${DEVKITARM_VER}"
DEVKITPRO_BASEDIR="dkarm-eabi"
inherit devkitpro

export DEVKITARM="${DEVKITPRO}/devkitARM"
