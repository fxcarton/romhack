# @ECLASS: devkitpro.eclass

export DEVKITPRO="/opt/devkitpro"

devkitpro_buildscripts_uri() {
	printf '%s -> %s' \
		"https://github.com/devkitPro/buildscripts/archive/${DEVKITPRO_PACKAGE}_r${DEVKITPRO_PACKAGE_VER}.tar.gz" \
		"${DEVKITPRO_PACKAGE}-buildscripts-r${DEVKITPRO_PACKAGE_VER}.tar.gz"
}

devkitpro_apply_patch() {
	local patchdir="${WORKDIR}/buildscripts-${DEVKITPRO_PACKAGE}_r${DEVKITPRO_PACKAGE_VER}/${DEVKITPRO_BASEDIR}/patches"
	local patchfile="${patchdir}/${1:-${P}.patch}"
	if ! test -d "${patchdir}"; then
		die "patch dir '${patchdir}' does not exist"
	fi
	if test -f "${patchfile}"; then
		einfo "Applying devkitpro patch ${patchfile}"
		eapply "${patchfile}" || die "failed to apply devkitpro patch"
	else
		einfo "No devkitpro patch in ${patchdir}"
	fi
}
