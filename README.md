# romhack ebuild repository
This repository contains ebuilds for various romhack/homebrew tools.
It also has ebuilds for the [devkitARM](https://devkitpro.org/) toolchain, as well as cross-compiled libraries for devkitarm.

## devkitARM

**DISCLAIMER:** If you do the following, **please report bugs to [this repository](https://framagit.org/fxcarton/romhack), NOT upstream**.
Upstream does not support custom builds of the toolchain, so any bugs encountered to anything build with the toolchain from this repository must be filed against this repository.

### Overview

This repository provides a way to install the devkitARM toolchain similar to how cross-toolchain as installed in gentoo.
This means that the crossdev wrappers for emerge (here installed as `dkarm-emerge`) allows to (or at least try to) cross-compile any packages.
This system provides the same functionality as devkitPro's portlibs system, except that is is not restricted to some packages, and thus any packages in ::gentoo can be potentially used.
Portage's convenient patch system can be used to add patches for packages when needed.

This very nice cross-compilation support in Gentoo is the motivation for packaging the devkitARM "the Gentoo way".
I believe it offers nice benefits to using the official binaries with `pacman` in Gentoo systems.
However, as noted above, custom builds are not supported by the devkitPro team.
Thus, only use this if:

- you know what you're doing
- have experience with gentoo's crossdev
- do not annoy the devkitPro team, and report bugs here instead

### Installing the toolchain

First, the toolchain is installed with the following commands:
```
# 1) Install binutils, the minimal compiler, and libc headers
USE="stage1" emerge -1a devkitarm/gcc
# 2) Install full compiler
emerge -1a devkitarm/gcc
# 3) Remove sys-include (important: this is needed)
emerge -ac devkitarm/newlib-headers
# 4) Install full toolchain
emerge -a devkitarm/devkitarm
```

Then, the base libraries are installed for each system with:
```
dkarm-emerge 3ds -a @system
dkarm-emerge nds -a @system
dkarm-emerge gba -a @system
dkarm-emerge gp32 -a @system
```

### Using the cross-compilation environment
Specific libraries can be installed with the cross-compilation environment. Example:
```
dkarm-emerge nds -a sys-libs/zlib
dkarm-emerge 3ds -a sys-libs/zlib
```

Note that some will require patches.
You may want to install `devkitarm/port-patches` from this repository to install patches provided by the devkitPro team.
This will install patches in the `/etc/portage/patches` of the devkitarm sysroots.
They may not match package versions in ::gentoo, so renaming and/or tweaking may be needed.
But they provide a very good basis for that.
