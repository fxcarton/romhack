# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C Library for Gamepark GP32"
HOMEPAGE="https://github.com/devkitPro/libmirko"
SRC_URI="https://github.com/devkitPro/libmirko/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="arm"
IUSE=""

BDEPEND="devkitarm/devkitarm-base[gp32]"
DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
	doheader -r include/*
	dolib.a lib/*.a
}
