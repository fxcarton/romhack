# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="FTP server"
HOMEPAGE="https://github.com/mtheall/ftpd"
SRC_URI="https://github.com/mtheall/ftpd/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="arm"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[3ds] >=app-3ds/makerom-0.18 app-3ds/bannertool"

src_compile() {
	emake release-{3dsx,cia}{,-classic}
}

src_install() {
	dobin 3ds/ftpd.{3dsx,cia} 3ds-classic/ftpd-classic.{3dsx,cia}
}
