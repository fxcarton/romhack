# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="3DS homebrew menu (new version)"
HOMEPAGE="https://github.com/fincs/new-hbmenu"
SRC_URI="https://github.com/fincs/new-hbmenu/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="arm"

DEPEND="dev-libs/tinyxml2 sys-libs/zlib"
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[3ds]"

S="${WORKDIR}/new-${P}"

src_compile() {
	emake RELEASE=1
}

src_install() {
	dobin boot.3dsx
}
