# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Open source title manager for the 3DS"
HOMEPAGE="https://github.com/Steveice10/FBI"
BUILDTOOLS="4524b3a324ae8e9dcaf80c4fe3694bc63628de22"
SRC_URI="https://github.com/Steveice10/FBI/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/Steveice10/buildtools/archive/${BUILDTOOLS}.tar.gz -> steveice10-buildtools-${BUILDTOOLS}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="arm"

DEPEND="net-misc/curl net-libs/mbedtls dev-libs/jansson sys-libs/zlib"
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[3ds] >=app-3ds/makerom-0.18 app-3ds/bannertool"

S="${WORKDIR}/FBI-${PV}"

src_prepare() {
	rmdir buildtools || die
	mv "${WORKDIR}/buildtools-${BUILDTOOLS}" buildtools || die
	#sed 's/^\t@/\t/' -i buildtools/make_base || die
	export LIBRARY_DIRS="${PORTLIBS_PREFIX}"
	default
}

src_compile() (
	unset CATEGORY
	emake
)

src_install() {
	dobin output/3ds-arm/{3ds/FBI/FBI.3dsx,FBI.cia,FBI.3ds}
}
