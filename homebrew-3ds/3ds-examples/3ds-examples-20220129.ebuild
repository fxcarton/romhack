# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Nintendo 3DS example code"
HOMEPAGE="https://github.com/devkitpro/3ds-examples"
SRC_URI="https://github.com/devkitpro/3ds-examples/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="arm"
IUSE="box2d modplug opus"

DEPEND="
	modplug? ( media-libs/libmodplug )
	opus? ( media-libs/opusfile )
	box2d? ( games-engines/box2d )"
RDEPEND="${DEPEND}"
BDEPEND="devkitarm/devkitarm[3ds] devkitarm/pkg-config"

src_prepare() {
	default
	sed -i '/export LD[ \t]*:=[ \t]*\$(CC)/s/\$(CC)/$(CXX)/' audio/modplug-decoding/Makefile || die
	sed -i 's/pkg-config.*--libs/& --static/' audio/opus-decoding/Makefile || die
	use box2d || rm physics/box2d/Makefile || die
	use modplug || rm audio/modplug-decoding/Makefile || die
	use opus || rm audio/opus-decoding/Makefile || die
}

src_install() {
	dobin bin/*.3dsx
}
