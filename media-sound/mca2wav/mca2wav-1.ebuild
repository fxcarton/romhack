# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

COMMIT=6033eca
DESCRIPTION="MCA to WAV converter"
HOMEPAGE="https://github.com/mhdasding/mca2wav"
SRC_URI="https://github.com/mhdasding/mca2wav/tarball/${COMMIT} -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}/mhdasding-mca2wav-${COMMIT}"

src_prepare() {
	sed -e '/^\tgcc .*-c/{s/.*-c \([^ ]*\).c/\1.o: mca.h/;H;d}' -e '/^mca2wav:/s/\.c/.o/g' -e '/^mca2wav:/s/[^ ]*.h//' -e "/^\tgcc /{s/gcc/$(tc-getCC)/;G}" -i Makefile || die "Makefile patch failed"
	eapply_user
}

src_install() {
	dobin mca2wav
}
