# Copyright 2018-2022 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="GBA homebrew tools (gbafix, gbalzss, gbfs, insgbfs, lsgbfs, ungbfs)"
HOMEPAGE="https://github.com/devkitPro/gba-tools"
SRC_URI="https://github.com/devkitPro/gba-tools/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+ GPL-3+"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
	eapply_user
}
