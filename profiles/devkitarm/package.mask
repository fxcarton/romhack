3ds-libs/citro2d
3ds-libs/citro3d
3ds-libs/libctru
homebrew-3ds/3ds-examples
homebrew-3ds/fbi
homebrew-3ds/ftpd
homebrew-3ds/hbmenu

nds-libs/dswifi
nds-libs/libfat
nds-libs/libfilesystem
nds-libs/libnds
nds-libs/maxmod
homebrew-nds/nds-examples
homebrew-nds/ftpd

gba-libs/libfat
gba-libs/libgba
gba-libs/maxmod
homebrew-gba/gba-examples

gp32-libs/libmirko
homebrew-gp32/gp32-examples
